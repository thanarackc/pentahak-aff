import { config } from '~/assets/constants/config'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default ({ app }, inject) => {
  inject('myConfig', () => config)
}
